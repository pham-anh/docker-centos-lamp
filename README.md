# How to use

```shell
git clone https://gitlab.com/pham-anh/docker-centos-lamp.git

cd docker-centos-lamp/c7-systemd
docker build --rm -t local/c7-systemd .

cd ../main
docker build --rm -t centos-lamp .

docker run -itd -e container=docker --cap-add SYS_ADMIN --tmpfs /run -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 80:80 centos-lamp
```


# Verify the container is running

```
anhpham89@testing $ docker container ls -a
CONTAINER ID        IMAGE                    COMMAND             CREATED             STATUS                      PORTS                NAMES
e4034e1f895b        centos-lamp            "/usr/sbin/init"    11 seconds ago      Up 10 seconds               0.0.0.0:80->80/tcp   admiring_galileo
```

Access `http://server_ip/`, see the Testing 123 page

Access `http://server_ip/phpinfo.php`, see PHP info


# Guidance is from
* https://hub.docker.com/_/centos/
* https://github.com/CentOS/sig-cloud-instance-images/issues/54#issuecomment-355046834


